import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HistoryPenjualanPage } from './history-penjualan';

@NgModule({
  declarations: [
    HistoryPenjualanPage,
  ],
  imports: [
    IonicPageModule.forChild(HistoryPenjualanPage),
  ],
})
export class HistoryPenjualanPageModule {}
