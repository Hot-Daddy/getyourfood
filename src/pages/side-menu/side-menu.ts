import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, Platform, ToastController } from 'ionic-angular';
import { AdminHomePage } from '../admin-home/admin-home';
import { AdminAddMerchantPage } from '../admin-add-merchant/admin-add-merchant';
import { AdminPage } from '../admin/admin';
import { AdminHistoryPenjualanPage } from '../admin-history-penjualan/admin-history-penjualan';
import { AdminGenerateBarcodePage } from '../admin-generate-barcode/admin-generate-barcode';
import { LoginPage } from '../login/login';
import firebase, { auth } from 'firebase';
import { HomePage } from '../home/home';
import { Geolocation } from '@ionic-native/geolocation';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';


@IonicPage()
@Component({
  selector: 'page-side-menu',
  templateUrl: 'side-menu.html',
})
export class SideMenuPage {

  rootPage:any
 
  data: any
  adminHome = AdminHomePage;
  adminAddMerchant = AdminAddMerchantPage;
  adminUser = AdminPage;
  adminHistory = AdminHistoryPenjualanPage;
  adminBarcode = AdminGenerateBarcodePage;
  logoutPage = LoginPage;
  homePage = HomePage;
  adminMenu: any = true;
  privillage: any;
  static tampung;

  locationIsSet = false;
  isOnRadius = false;

  latAssignment: any;
  lngAssignment: any;
  
  scannedCode = null;
  curLatitude = null;
  curLongitude = null;

  lat : any;
  lng : any;
  distanceBetween: any;
  

  constructor(public navCtrl: NavController, public navParams: NavParams, private menuCtrl: MenuController,
    public geolocation: Geolocation, public platform: Platform, public scanner: BarcodeScanner, private toast: ToastController) {
      this.geolocation.getCurrentPosition().then((resp) => {
        this.curLatitude = resp.coords.latitude;
        this.curLongitude = resp.coords.longitude;
        this.locationIsSet = true;
     }).catch((error) => {
        alert(error);
     });
  }

  ionViewDidLoad() {
    const unsub = firebase.auth().onAuthStateChanged(user=> {
      console.log("masuk");
      // auth().signOut()
      if (!user) {
        this.rootPage = LoginPage
        unsub()
      } else {
        console.log(user.email);
        
        if (user.email.toString() == "admin1@mail.com") {
          this.adminMenu = true
          this.rootPage = AdminHomePage
        } else {
          firebase.database().ref('users/').child(auth().currentUser.uid).once('value', (snapshot)=> {
            console.log(snapshot.val());
            
            // console.log(snapshot);
            this.data = snapshot.val() as [any,any]
            this.privillage = this.data["privillage"] as string
            // console.log(this.privillage);
           
            
          }).then((user)=>{ 
            if(this.privillage == "pembeli"){
              this.scanner.scan().then(barcodeData => {
                this.scannedCode = barcodeData.text;
                // this.scannedCode.split(",");

                // this.latAssignment = -6.257119;
                // this.lngAssignment = 106.619326;
                
                // this.latAssignment = -6.257119;
                // this.lngAssignment = 106.619326;
                // this.lat = parseFloat(this.curLatitude);
                // this.lng = parseFloat(this.curLongitude);
                // console.log("langitude" + this.lat);
                // console.log("longitude" + this.lng);
                // console.log("langitude code" + this.latAssignment);
                // console.log("longitude code" + this.lngAssignment);
                // this.distanceBetween = (this.getDistance(this.lat,this.lng, this.latAssignment, this.lngAssignment, 'spherical'));
                // console.log("distance" + this.distanceBetween);
                // if(this.distanceBetween < 1){
                //   this.isOnRadius = true;
                  localStorage.setItem('storedData', this.privillage);
                // }
                // else{
                //   console.log("error out of distance");
                // }
              })
            }else if(this.privillage == "penjual"){
              localStorage.setItem('storedData', this.privillage);
            }
            
            this.adminMenu = false
            this.rootPage = HomePage
            unsub()
          })
        }
      }
    })
  }

  onLoad(page: any){
    this.navCtrl.push(page);
    this.menuCtrl.close();
  }
  onLogout() {
    this.navCtrl.setRoot(LoginPage);
    this.menuCtrl.close();
    firebase.auth().signOut() 
    localStorage.clear();
  }

  degToRad(n) {
    return n * Math.PI / 180;
  }

  radToDeg(n) {
    return n * 180 / Math.PI;
  }

  getDistance(lat1, lon1, lat2, lon2, mode) {	
    var R = 6371; // Earth radius in km
    
    switch(mode)
    {	
      case 'spherical':
      default:
        var dLon = this.degToRad(lon2 - lon1);
        lat1 = this.degToRad(lat1);
        lat2 = this.degToRad(lat2);
        var d = Math.acos(Math.sin(lat1) * Math.sin(lat2) + Math.cos(lat1) * Math.cos(lat2) * Math.cos(dLon)) * R;
      break;	
      
      case 'haversine':
        var dLat = this.degToRad(lat2 - lat1);
        var dLon = this.degToRad(lon2 - lon1);
        lat1 = this.degToRad(lat1);
        lat2 = this.degToRad(lat2);
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2); 
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)); 
        var d = R * c;
      break;	
      
      case 'rectangle':
        var x = this.degToRad(lon2 - lon1) * Math.cos(this.degToRad(lat1 + lat2) / 2);
        var y = this.degToRad(lat2 - lat1);
        var d = Math.sqrt(x * x + y * y) * R;
      break;	
    }
      
    return d;	
  }

}
