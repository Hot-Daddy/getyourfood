import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { UserMenusPage } from '../user-menus/user-menus';
import { FirebaseListObservable, AngularFireDatabase } from 'angularfire2/database-deprecated';
import firebase from 'firebase';
import { auth } from 'firebase/app';

@Component({
  selector: 'page-user',
  templateUrl: 'user.html'
})
export class UserPage {
  price: any
  name:any
  merchantList: FirebaseListObservable<any>
  data: FirebaseListObservable<any>
  arrData = []
  constructor(public navCtrl: NavController, public fdb: AngularFireDatabase) {
    
    this.merchantList = this.fdb.list('/penjual/');
    // console.log(this.merchantList);
    
  }
  async ionViewWillEnter() {
    this.getUserData()
  }
  getUserData() {
    firebase.database().ref('/pembeli/'+ auth().currentUser.uid).once('value', snapshot=>{
      let data = snapshot.val()
      let email = data["email"] as string
      let saldo = parseInt(data["saldo"])

      this.name = email
      this.price = saldo
    })
  }
  findmenus(key){
    
    this.data = this.fdb.list('menu/'+key)
    this.data.subscribe(_data=>{
      this.arrData = _data;
      
      this.navCtrl.push(UserMenusPage,{allData: this.arrData, userID: key, saldo: this.price})
    });
  }

}
