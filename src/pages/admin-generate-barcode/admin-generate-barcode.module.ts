import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminGenerateBarcodePage } from './admin-generate-barcode';

@NgModule({
  declarations: [
    AdminGenerateBarcodePage,
  ],
  imports: [
    IonicPageModule.forChild(AdminGenerateBarcodePage),
  ],
})
export class AdminGenerateBarcodePageModule {}
