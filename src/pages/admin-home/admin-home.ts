import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { AdminMerchantModalPage } from '../admin-merchant-modal/admin-merchant-modal';
import { FirebaseListObservable, AngularFireDatabase } from 'angularfire2/database-deprecated';
import { AngularFireAuth } from 'angularfire2/auth';
import firebase from 'firebase';



@IonicPage()
@Component({
  selector: 'page-admin-home',
  templateUrl: 'admin-home.html',
})
export class AdminHomePage {

  merchantList: FirebaseListObservable<any>
  allArrData = []
  arrData = []
  email: any
  password: any
  namaToko: any
  saldo: any
  description: any

  queryText: string;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              private modalCtrl: ModalController,
              private afAuth: AngularFireAuth,
              public fdb: AngularFireDatabase) {

    this.merchantList = this.fdb.list('/penjual/');
      this.merchantList.subscribe(_data=>{
        // console.log(_data);
        
        this.allArrData = _data;
        this.arrData = _data
        // console.log(this.arrData);
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminHomePage');
    console.log(this.allArrData);
  }

  modalDetailsMerchant(key, deskripsi, email, namaToko, saldo){
    console.log(deskripsi, key, email, namaToko, saldo);
    
    let modal = this.modalCtrl.create(AdminMerchantModalPage, {
      id: key,
      deskripsi: deskripsi,
      email: email,
      namaToko: namaToko,
      saldo: saldo
    });
    modal.present();

  }

  searchMerchant(){
    let queryToLower = this.queryText.toLowerCase();
    this.arrData = []
    this.allArrData.forEach(element => {
      if (queryToLower == "") {
        this.arrData = this.allArrData
      } 
      else if (element.namaToko.toLowerCase().includes(queryToLower) == true) {
        this.arrData.push(element)
      }
      else if(element.email.toLowerCase().includes(queryToLower) == true){
        this.arrData.push(element)
      }
    });
    
  }

}
