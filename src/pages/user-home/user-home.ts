import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserPage } from '../user/user';
import { UserMenusPage } from '../user-menus/user-menus';
import { UserHistoryPage } from '../user-history/user-history';

/**
 * Generated class for the UserHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-user-home',
  templateUrl: 'user-home.html'
})
export class UserHomePage{
  page1: any = UserPage;
  page2: any = UserHistoryPage;
  constructor(public navCtrl: NavController) {

  }
}
