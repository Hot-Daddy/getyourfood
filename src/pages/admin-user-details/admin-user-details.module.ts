import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminUserDetailsPage } from './admin-user-details';

@NgModule({
  declarations: [
    AdminUserDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminUserDetailsPage),
  ],
})
export class AdminUserDetailsPageModule {}
