import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import {DeskripsiModalPage} from '../../pages/deskripsi-modal/deskripsi-modal'
import { Observable } from 'rxjs/Observable';
import { FirebaseListObservable } from 'angularfire2/database-deprecated';
import firebase from 'firebase';

/**
 * Generated class for the UserMenusPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-menus',
  templateUrl: 'user-menus.html',
})
export class UserMenusPage {
  // menus: FirebaseListObservable<any>
  menus = [];
  userID: string;
  namaToko: any
  saldo:any
  private qty = 0;
  constructor(private modal: ModalController, public navParams: NavParams) {
    this.menus = navParams.get('allData')
    this.userID = navParams.get('userID')
    this.saldo = navParams.get('saldo')
    firebase.database().ref('penjual/'+this.userID).once('value',snapshot=>{
      
      let data = snapshot.val() as [any,any]
      this.namaToko = data["namaToko"]
      console.log(this.namaToko);
      
    })
  }

  deskripsiKlik(id, menu, harga, desc){  
    
    if (this.namaToko != undefined) {
      const descModal = this.modal.create(DeskripsiModalPage, {
        userID: this.userID, 
        menuID : id,
        menu : menu,
        harga : harga,
        qty: '1',
        desc : desc,
        namaToko: this.namaToko,
        saldo: this.saldo
      });
      descModal.present();
    } 
  }
  increment(){
    this.qty++;
  }
  decrement(){
    if(this.qty > 0){
      this.qty--;
    }
  }
}