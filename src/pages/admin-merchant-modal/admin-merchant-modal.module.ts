import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminMerchantModalPage } from './admin-merchant-modal';

@NgModule({
  declarations: [
    AdminMerchantModalPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminMerchantModalPage),
  ],
})
export class AdminMerchantModalPageModule {}
