import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
import firebase, { auth } from 'firebase';

/**
 * Generated class for the UserHistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-history',
  templateUrl: 'user-history.html',
})

export class UserHistoryPage {
  disable : boolean = true;
  hidden : boolean = false;
  idHistory = []
  historyResult = []
  saldo = 0
  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public toastCtrl: ToastController) {
  
  }
  ionViewWillEnter() {
    this.historyResult = []
    this.getPembeliHistory()
  }
  showComplete(){
    this.hidden = true;
  }

  getPembeliHistory() {
    firebase.database().ref('/pembeli/'+auth().currentUser.uid+'/history').on('child_added', snapshot => {
      
      this.idHistory.push(snapshot.key) 
      // console.log(this.idHistoryArr);
      let data = snapshot.val() as [any,any]
      let idPenjual = data["idPenjual"] as string
      let harga = parseInt(data["harga"])
      let qty = parseInt(data["qty"])
      let totalHarga = parseInt(data["totalHarga"])
      let namaMenu = data["namaMenu"] as string
      let status = data["status"] as string
      let waktu = data["waktu"] as string
      let namaToko = data["namaToko"] as string
      this.historyResult.push({
        idPenjualKey: idPenjual,
        hargaKey: harga,
        qtyKey: qty,
        totalHargaKey: totalHarga,
        namaMenuKey: namaMenu,
        statusKey: status,
        waktuKey: waktu,
        namaTokoKey: namaToko,
      })
      console.log(status);
    })
    
    
  }
  presentToast(){
    let toast = this.toastCtrl.create({
      message: 'Enjoy your meal, thank you for using our application',
      duration: 3000,
      position: 'bottom'
    });
    toast.onDidDismiss(() => {
      console.log("ilang nih toastnya");
    });
    toast.present();
  }

  mehalert(){
    let alert = this.alertCtrl.create({
      subTitle: 'Do not forget to take your food',
      buttons: ['Dismiss']
    });
    alert.present();
  }
  pickUpConfirm(index, idPenjual, totalHarga){
    
    let alert = this.alertCtrl.create({
      title: 'Take your food ?',
      message: 'Are you going to take your food ? Make sure it is ready to eat',
      buttons: [
        {
          text: 'Yes, I am sure',
          handler: () => {
            firebase.database().ref('/penjual/'+idPenjual+'/history/'+this.idHistory[index]).update({
              "status":"already pickup"
            })
            firebase.database().ref('/pembeli/'+auth().currentUser.uid+'/history/'+this.idHistory[index]).update({
              "status":"already pickup"
            })
            firebase.database().ref('/penjual/'+idPenjual).once('value',snaphot=>{
              let data = snaphot.val() as [any,any]
              let saldo = parseInt(data["saldo"])
              this.saldo = saldo
            })
            .then(data=>{
              firebase.database().ref('/penjual/'+idPenjual).update({
                "saldo": this.saldo+ totalHarga
              })
              this.presentToast();
              this.historyResult = []
              this.getPembeliHistory()
            })
            
          }
        },
        {
          text: 'Meh, later',
          handler: () => {
            this.mehalert();
          }
        }
      ]
    });
    alert.present();
  }
  cancelConfirm(){
    let alert = this.alertCtrl.create({
      title: 'Do you want to cancel ?',
      message: 'Cancelling your order will get you only 50% of refund, make sure you are okay with this !',
      buttons: [
        {
          text: 'Lost 50% of money',
          handler: ()=>{
            console.log("ilang duit 50%");
          }
        },
        {
          text: 'No, I will wait',
          handler: () =>{
            console.log("ga kenapa napa");
          }
        }
      ]
    });
    alert.present();
  }
}