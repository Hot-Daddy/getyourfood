import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminHistoryPenjualanPage } from './admin-history-penjualan';

@NgModule({
  declarations: [
    AdminHistoryPenjualanPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminHistoryPenjualanPage),
  ],
})
export class AdminHistoryPenjualanPageModule {}
