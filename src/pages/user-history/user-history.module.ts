import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserHistoryPage } from './user-history';

@NgModule({
  declarations: [
    UserHistoryPage,
  ],
  imports: [
    IonicPageModule.forChild(UserHistoryPage),
  ],
})
export class UserHistoryPageModule {}
