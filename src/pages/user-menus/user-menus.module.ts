import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserMenusPage } from './user-menus';

@NgModule({
  declarations: [
    UserMenusPage,
  ],
  imports: [
    IonicPageModule.forChild(UserMenusPage),
  ],
})
export class UserMenusPageModule {}
