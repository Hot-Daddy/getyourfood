import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { FirebaseListObservable, AngularFireDatabase } from 'angularfire2/database-deprecated';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { HomePage } from '../home/home';

/**
 * Generated class for the PenjualEditMenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-penjual-edit-menu',
  templateUrl: 'penjual-edit-menu.html',
})
export class PenjualEditMenuPage {
  menus : FirebaseListObservable<any>

  tampMenu = {
    id: '',
    menu: '',
    harga: '',
    desc: ''
  }
  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public viewCtrl: ViewController, private fdb: AngularFireDatabase, private toast: ToastController) {
      
    this.menus = fdb.list('/menu')
    this.tampMenu.id = this.navParams.get('key')
    this.tampMenu.menu = this.navParams.get('menu')
    this.tampMenu.harga = this.navParams.get('harga')
    this.tampMenu.desc = this.navParams.get('desc')
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PenjualEditMenuPage');
  }
  closeModal(){
    this.viewCtrl.dismiss();
  }
  btnUpdateMenu(id, menu, harga, desc){
    this.menus.update(id = this.tampMenu.id, {
      menu : this.tampMenu.menu ,
      harga : this.tampMenu.harga ,
      deskripsi : this.tampMenu.desc ,
    }).then(updateMenu =>{
      this.viewCtrl.dismiss();
      this.toast.create({
        message: `Data Updated`,
        duration: 1000
      }).present(); 
    }, error=>{console.log(error)});
  }
}
