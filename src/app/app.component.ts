import { Component, ViewChild } from '@angular/core';
import { Platform, NavController, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { AdminHomePage } from '../pages/admin-home/admin-home';
import { AdminGenerateBarcodePage } from '../pages/admin-generate-barcode/admin-generate-barcode';
import { AdminAddMerchantPage } from '../pages/admin-add-merchant/admin-add-merchant';
import { AdminPage } from '../pages/admin/admin';
import { AdminHistoryPenjualanPage } from '../pages/admin-history-penjualan/admin-history-penjualan';
import firebase, { auth } from 'firebase';
import { AngularFireAuth } from 'angularfire2/auth';
import { Config } from 'ionic-angular/config/config';
import { SideMenuPage } from '../pages/side-menu/side-menu';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any
 
  adminHome = AdminHomePage;
  adminAddMerchant = AdminAddMerchantPage;
  adminUser = AdminPage;
  adminHistory = AdminHistoryPenjualanPage;
  adminBarcode =AdminGenerateBarcodePage;
  logoutPage = LoginPage
  adminMenu: any = true

  @ViewChild('sideMenuContent') nav: NavController;
  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private menuCtrl: MenuController) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

      var config = {
        apiKey: "AIzaSyAg2eYlLfreyrRYcbpyJcGoXYr6jcDPrGM",
        authDomain: "master-ionic.firebaseapp.com",
        databaseURL: "https://master-ionic.firebaseio.com",
        projectId: "master-ionic",
        storageBucket: "master-ionic.appspot.com",
        messagingSenderId: "457998989746"
      };
      firebase.initializeApp(config);
      // const unsub = firebase.auth().onAuthStateChanged(user=> {
      //   if (!user) {
      //     this.rootPage = LoginPage
      //     unsub()
      //   } else {
      //     if (user.email.toString() == "admin1@mail.com") {
      //       this.adminMenu = true
      //       this.rootPage = AdminHomePage
      //     } else {
      //       this.adminMenu = false
      //       var stateData = user.email.split("@")
      //       var stateArray = stateData[1].split(".")
      //       var state = stateArray[0]
      //       if (state == "store") {
      //         this.rootPage = HomePage
      //       } else {
      //         this.rootPage = HomePage
      //       }
      //     }
      //     unsub()
      //   }
      // })
      this.rootPage = SideMenuPage
    });
  }
  // onLoad(page: any){
  //   this.nav.setRoot(page);
  //   this.menuCtrl.close();
  // }
  // onLogout() {
  //   this.nav.setRoot(LoginPage);
  //   this.menuCtrl.close();
  //   firebase.auth().signOut()
  // }
}

