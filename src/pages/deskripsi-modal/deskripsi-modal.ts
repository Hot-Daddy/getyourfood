import { Component } from '@angular/core';
import { IonicPage, NavParams, ViewController, AlertController } from 'ionic-angular';
import { DatePipe } from '@angular/common';
import firebase from 'firebase';
import { auth } from 'firebase/app';

/**
 * Generated class for the DeskripsiModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-deskripsi-modal',
  templateUrl: 'deskripsi-modal.html',
})

export class DeskripsiModalPage {
  menus : any;
  namaToko: any
  saldo:any
  tempData = []
  qty = 1;
  tampMenu = {
    userID:'',
    menuID: '',
    menu: '',
    harga: '',
    desc: ''
  }
  harga = +this.tampMenu.harga;
  
  constructor(private  view: ViewController, public navParams: NavParams, public datepipe: DatePipe, private alertCtrl: AlertController) {
    this.menus = [
      {
        userID: this.tampMenu.userID = this.navParams.get('userID'),
        menuID :this.tampMenu.menuID = this.navParams.get('menuID'),
        picture: '/../../assets/imgs/pisgor.jpg', 
        name:  this.tampMenu.menu = this.navParams.get('menu'),
        deskripsi: this.tampMenu.desc = this.navParams.get('desc'), 
        harga: this.tampMenu.harga = this.navParams.get('harga')
      }
    ];
    console.log(this.navParams.get('userID')+ navParams.get('menuID'))
  // this.tampMenu.id = this.navParams.get('key')
  //   // this.tampMenu.menu = this.navParams.get('menu')
  //   // this.tampMenu.harga = this.navParams.get('harga')
  //   this.tampMenu.desc = this.navParams.get('desc')
    this.harga = +this.tampMenu.harga* this.qty;
    this.namaToko = this.navParams.get('namaToko')
    this.saldo = navParams.get('saldo')
  }
  decrement(){
    if(this.qty > 1){
      this.qty--;
      this.harga = +this.tampMenu.harga* this.qty;
    console.log(this.harga, this.tampMenu.harga);
    }
  }
  increment(){
    this.qty++;
    this.harga = +this.tampMenu.harga* this.qty;
    console.log(this.harga, this.tampMenu.harga);
  }
  closeDesc(){
    this.view.dismiss();
  }
  ionViewWillLoad() {
    const data = this.navParams.get('allData');
    firebase
  }

  buyButton(penjualID, menuID, namaMenu, deskripsi, qty, harga) {
    if (this.saldo < harga){
      let alert = this.alertCtrl.create({
        title: 'Your saldo is not enough',
        subTitle: 'please top-up your wallet',
        buttons: [{
          text:"OK",
          handler:() => {}
        }]
      });
      alert.present();
    } else {
      let date: string = new Date(new Date().getTime()).toString();
      let latest_date = this.datepipe.transform(date, 'd MMMM-yyyy, HH:mm')
      let dateParam = this.datepipe.transform(date, 'E-d-MMM-yyyy-HH-mm-ss-')
      firebase.database().ref('pembeli/'+auth().currentUser.uid+'/history/'+dateParam).set({
        namaMenu: namaMenu,
        qty: this.qty,
        harga: this.harga/this.qty,
        totalHarga: this.harga,
        waktu: latest_date,
        status: "not pickup",
        namaToko: this.namaToko,
        idPenjual: penjualID
      })
      firebase.database().ref('penjual/'+penjualID+'/history/'+dateParam).set({
        namaMenu: namaMenu,
        qty: this.qty,
        harga: this.harga/this.qty,
        totalHarga: this.harga,
        emailPembeli: auth().currentUser.email,
        waktu: latest_date,
        status: "not pickup",
        idPembeli: auth().currentUser.uid
      })
      .then(user=>{
        firebase.database().ref('/pembeli/'+auth().currentUser.uid).update({
          "saldo": this.saldo - harga
        }).then (user=>{
          let alert = this.alertCtrl.create({
            title: 'Success!',
            subTitle: 'Your order has been placed',
            buttons: [{
              text:"OK",
              handler:() => {this.view.dismiss()}
            }]
          });
          alert.present();
        })
      })
    }
  }
}
