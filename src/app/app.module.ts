import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

//Penjual Pages
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { SuperTabsModule } from 'ionic2-super-tabs';
import { HomePenjualanPage } from '../pages/home-penjualan/home-penjualan';
import { HistoryPenjualanPage } from '../pages/history-penjualan/history-penjualan';
import { LoginPage } from '../pages/login/login';
//end penjual//
//Admin PAGES//
import { AdminAddMerchantPage } from '../pages/admin-add-merchant/admin-add-merchant';
import { AdminHomePage } from '../pages/admin-home/admin-home';
import { AdminPage } from '../pages/admin/admin';
import { AdminHistoryPenjualanPage } from '../pages/admin-history-penjualan/admin-history-penjualan';
import { AdminMerchantModalPage } from '../pages/admin-merchant-modal/admin-merchant-modal';
import { AdminUserDetailsPage } from '../pages/admin-user-details/admin-user-details';
import { AdminGenerateBarcodePage } from '../pages/admin-generate-barcode/admin-generate-barcode';

import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { BarcodeScanner} from '@ionic-native/barcode-scanner';
//End Admin Pages//

//User Pages
import { UserPage } from '../pages/user/user';
import { UserMenusPage } from '../pages/user-menus/user-menus';
import { UserHomePage } from '../pages/user-home/user-home';
import { UserHistoryPage } from '../pages/user-history/user-history';
import { DeskripsiModalPage } from '../pages/deskripsi-modal/deskripsi-modal';
//End User Page

//firebase
import { FirebaseProvider } from '../providers/firebase/firebase';
import { HttpModule } from '@angular/http';

import { AngularFireDatabaseModule } from 'angularfire2/database-deprecated';
import { AngularFireModule} from 'angularfire2';
import { AngularFireAuth, AngularFireAuthModule } from 'angularfire2/auth';
import { PenjualAddMenuPage } from '../pages/penjual-add-menu/penjual-add-menu';
import { PenjualEditMenuPage } from '../pages/penjual-edit-menu/penjual-edit-menu';
import { Camera } from '@ionic-native/camera';
import { SideMenuPage } from '../pages/side-menu/side-menu';
import { DeskripsiModalPageModule } from '../pages/deskripsi-modal/deskripsi-modal.module';
import { DatePipe } from '@angular/common';
//end firebase

import { Geolocation } from '@ionic-native/geolocation';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    HomePenjualanPage,
    HistoryPenjualanPage,
    PenjualAddMenuPage,
    PenjualEditMenuPage,
    LoginPage,
    AdminHomePage,
    AdminAddMerchantPage,
    AdminPage,
    AdminHistoryPenjualanPage,
    AdminMerchantModalPage,
    AdminUserDetailsPage,
    AdminGenerateBarcodePage,
    SideMenuPage,
    UserPage,
    UserMenusPage,
    UserHomePage,
    UserHistoryPage
  ],
  imports: [
    AngularFireModule.initializeApp({
      apiKey: "AIzaSyAg2eYlLfreyrRYcbpyJcGoXYr6jcDPrGM",
      authDomain: "master-ionic.firebaseapp.com",
      databaseURL: "https://master-ionic.firebaseio.com",
      projectId: "master-ionic",
      storageBucket: "master-ionic.appspot.com",
      messagingSenderId: "457998989746"
    }),
    BrowserModule,
    IonicModule.forRoot(MyApp),
    SuperTabsModule.forRoot(),
    HttpModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    DeskripsiModalPageModule,
    NgxQRCodeModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    HomePenjualanPage,
    HistoryPenjualanPage,
    PenjualAddMenuPage,
    PenjualEditMenuPage, 
    LoginPage,
    AdminHomePage,
    AdminAddMerchantPage,
    AdminPage,
    AdminHistoryPenjualanPage,
    AdminMerchantModalPage,
    AdminUserDetailsPage,
    AdminGenerateBarcodePage,
    SideMenuPage,
    UserPage,
    UserMenusPage,
    UserHomePage,
    UserHistoryPage,
    DeskripsiModalPage
  ],
  providers: [
    Camera,
    BarcodeScanner,
    StatusBar,
    SplashScreen,
    FirebaseProvider,
    AngularFireAuth,
    FirebaseProvider,
    [DatePipe],
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    File,
    FileOpener,
    Geolocation

  ]
})
export class AppModule {}