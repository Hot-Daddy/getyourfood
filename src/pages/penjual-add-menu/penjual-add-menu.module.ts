import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PenjualAddMenuPage } from './penjual-add-menu';

@NgModule({
  declarations: [
    PenjualAddMenuPage,
  ],
  imports: [
    IonicPageModule.forChild(PenjualAddMenuPage),
  ],
})
export class PenjualAddMenuPageModule {}
