import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController,AlertController} from 'ionic-angular';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database-deprecated';
import firebase from 'firebase';

/**
 * Generated class for the AdminMerchantModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-admin-merchant-modal',
  templateUrl: 'admin-merchant-modal.html',
})
export class AdminMerchantModalPage {

  
    id: any
    deskripsi: any
    email: any
    namaToko: any
    saldo: any
    penjual : FirebaseListObservable<any>
  
  
    constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              private viewCtrl: ViewController, 
              private fdb: AngularFireDatabase, 
              private toast: ToastController,
              private alertCtrl: AlertController) {
    
    this.penjual = fdb.list('/penjual')
    this.id = this.navParams.get('id')
    this.email = this.navParams.get('email')
    this.namaToko = this.navParams.get('namaToko')
    this.saldo = this.navParams.get('saldo')
    this.deskripsi = this.navParams.get('deskripsi')
    console.log(this.deskripsi);
    
    
  }

  ionViewDidLoad() {
    // this.getDataPenjual()
    console.log('ionViewDidLoad AdminMerchantModalPage');
  }

  closeModal(){
    this.viewCtrl.dismiss();
  }
  btnUpdateMenu(id, deskripsi, namaToko){
    this.penjual.update(id = this.id, {
      deskripsi : this.deskripsi ,
      namaToko : this.namaToko
    }).then(updateMenu =>{
      this.viewCtrl.dismiss();
      this.toast.create({
        message: `Data Updated`,
        duration: 1000
      }).present(); 
    }, error=>{console.log(error)});
  }
  getDataPenjual() {
    firebase.database().ref('/penjual/').once('value', (snapshot) => {
      console.log(snapshot.val());
      let dict = snapshot.val()
      let temp = [];
      temp = snapshot.val();
      temp.forEach(function(element) {
        var username = element.key
        console.log(username);
      });
    })
  }

  presentPrompt(id,saldo) {
    let alert = this.alertCtrl.create({
      title: 'Tarik Saldo',
      message: 'Saldo yang dapat ditarik : ' + saldo,
      inputs: [
        {
          name: 'saldoTarik',
          value: 'Jumlah Tarik Saldo',
          type: 'number'
        },
        {
          name: 'idMerchant',
          value: id,
          type: 'hidden'
        },
        {
          name: 'saldoDB',
          value: saldo,
          type: 'hidden'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Confirm',
          handler: data => {
            console.log(data.saldoDB, data.saldoTarik);
            let saldoInsert = data.saldoDB - data.saldoTarik;
            if(saldoInsert >= 0){
              this.penjual.update(id = data.idMerchant, {
                saldo: saldoInsert
              }).then(updateMenu =>{
                //this.alertCtrl.dismiss();
                this.toast.create({
                  message: `Berhasil Tarik Saldo`,
                  duration: 1000
                }).present(); 
                this.viewCtrl.dismiss();

              }, error=>{console.log(error)});
            }
            else{
              this.toast.create({
                message: `Gagal Tarik Saldo`,
                duration: 1000
              }).present(); 
              this.viewCtrl.dismiss();
            }
            
          }
        }
      ],
      enableBackdropDismiss: false 
    });
    
    alert.present();
  }





}
