import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomePenjualanPage } from './home-penjualan';

@NgModule({
  declarations: [
    HomePenjualanPage,
  ],
  imports: [
    IonicPageModule.forChild(HomePenjualanPage),
  ],
})
export class HomePenjualanPageModule {}
