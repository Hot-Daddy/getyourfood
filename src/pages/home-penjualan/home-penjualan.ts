import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { PenjualAddMenuPage } from '../penjual-add-menu/penjual-add-menu';
import { AngularFireAuth } from 'angularfire2/auth';
import { FirebaseListObservable, AngularFireDatabase } from 'angularfire2/database-deprecated';
import { PenjualEditMenuPage } from '../penjual-edit-menu/penjual-edit-menu';
import firebase, { auth } from 'firebase';

/**
 * Generated class for the HomePenjualanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home-penjualan',
  templateUrl: 'home-penjualan.html',
})
export class HomePenjualanPage {
  menus : FirebaseListObservable<any>;
  data: any
  price: any
  name: any;
  constructor(public navCtrl: NavController,private afAuth: AngularFireAuth, 
  public navParams: NavParams, private modalCtrl: ModalController, private fdb: AngularFireDatabase) {

    this.menus = fdb.list('/menu/'+auth().currentUser.uid)
  }

  ionViewDidLoad() {
    this.afAuth.authState.subscribe(data=>{
      if(data.email && data.uid){
        this.name = data.email;
      }
    })
    console.log('ionViewDidLoad HomePenjualanPage');
  }

  ionViewWillEnter() {
    this.getUserSaldo()
    
    // this.data = this.fdb.list('/penjual/'+auth().currentUser.uid);
    // this.data.subscribe(_data=>{
      
    //   console.log(_data);
      
    //   this.data = _data as [any,any]
      
    //   this.price = this.data[4]["saldo"]
    //   console.log(this.price);
    // });
  }
  async getUserSaldo() {
    firebase.database().ref('penjual/'+auth().currentUser.uid).once('value',snapshot=>{
      console.log(snapshot.val());
      let data = snapshot.val() as [any, any]
      this.price = parseInt(data["saldo"])
    })
  }

  getDataUser() {
    this.fdb.list('/penjual/').subscribe
  }
  modalAddMenu(){
    let modal = this.modalCtrl.create(PenjualAddMenuPage);
    modal.present();
  }

  deleteOnSelected(id){
    this.menus.remove(id);
  }

  editOnSelected(id, menu, harga, desc){
    let modal = this.modalCtrl.create(PenjualEditMenuPage,{
      key : id,
      menu : menu,
      harga : harga,
      deskripsi : desc
    });
    modal.present();
  }
  
}
