import { Component, ViewChild } from '@angular/core';
import { NavController, Slides, ToastController, NavParams, AlertController } from 'ionic-angular';
import { HistoryPenjualanPage } from '../history-penjualan/history-penjualan';
import { HomePenjualanPage } from '../home-penjualan/home-penjualan';
import { AngularFireAuth } from 'angularfire2/auth';
import firebase, { auth } from 'firebase';
import { AngularFireDatabase, FirebaseObjectObservable } from 'angularfire2/database-deprecated';
import { UserHistoryPage } from '../user-history/user-history';
import { UserPage } from '../user/user';
import { SideMenuPage } from '../side-menu/side-menu';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  page1: any 
  page2: any
  name: any
  data: any
  privillage: any
  uid = auth().currentUser.uid

  stores: FirebaseObjectObservable<any>

  scannedCode = null;

  homePenjual = HomePenjualanPage;
  historyPenjual = HistoryPenjualanPage;
  homeUser = UserPage;
  historyUser = UserHistoryPage;


  constructor(public navCtrl: NavController, private navParam: NavParams, private afAuth: AngularFireAuth,
     private toast: ToastController, public db: AngularFireDatabase, public scanner: BarcodeScanner,private alertCtrl: AlertController) {
    var page1Data = localStorage.getItem('storedData');
    console.log(page1Data);
    
    if (page1Data == "penjual") {
      this.page1 = this.homePenjual;
      this.page2 = this.historyPenjual;
    } else if (page1Data   == "pembeli") {
      this.page1 = this.homeUser;
      this.page2 = this.historyUser;
    }
  }
  
  farAway(){
    this.navCtrl.push(LoginPage);
    auth().signOut();
  }

  ionViewWillLoad(){
    this.afAuth.authState.subscribe(data=>{
      if(data.email && data.uid){
        this.name = data.email;
      }
      if(data.email){
        this.toast.create({
              message: `Welcome to GYF, ${data.email}`,
              duration: 2000
            }).present();  
      }else{
        this.toast.create({
          message: `Data Not Found`,
          duration: 2000
        }).present();
      }
    })
  }
}
