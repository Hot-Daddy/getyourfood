import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database-deprecated';
import { DatePipe } from '@angular/common';
import { auth } from 'firebase';
import firebase from 'firebase';

/**
 * Generated class for the HistoryPenjualanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-history-penjualan',
  templateUrl: 'history-penjualan.html',
})
export class HistoryPenjualanPage {
  historyResult = []
  constructor(public navCtrl: NavController, public navParams: NavParams, public fdb: AngularFireDatabase, public datepipe: DatePipe) {
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HistoryPenjualanPage');
  }
  ionViewWillEnter() {
    this.historyResult = []
    this.getpenjualHistory()
  }

  getpenjualHistory() {
    firebase.database().ref('/penjual/'+auth().currentUser.uid+'/history').once('child_added', snapshot => {
      
      let data = snapshot.val() as [any,any]
      let emailPembeli = data["emailPembeli"] as string
      let harga = parseInt(data["harga"])
      let qty = parseInt(data["qty"])
      let totalHarga = parseInt(data["totalHarga"])
      let namaMenu = data["namaMenu"] as string
      let status = data["status"] as string
      let waktu = data["waktu"] as string
      this.historyResult.push({
        emailPembeliKey: emailPembeli,
        hargaKey: harga,
        qtyKey: qty,
        totalHargaKey: totalHarga,
        namaMenuKey: namaMenu,
        statusKey: status,
        waktuKey: waktu
      })
    })
  }
}