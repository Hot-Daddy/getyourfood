import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { FileOpener } from '@ionic-native/file-opener';
import { File } from '@ionic-native/file';

import pdfMake from 'pdfmake/build/pdfMake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@IonicPage()
@Component({
  selector: 'page-admin-generate-barcode',
  templateUrl: 'admin-generate-barcode.html',
})
export class AdminGenerateBarcodePage {

  qrData = null;
  createdCode = null;
  scannedCode = null;

  pdfObj = null;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public scanner: BarcodeScanner, private file: File, private fileOpener: FileOpener,
    private plt: Platform) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminGenerateBarcodePage');
  }

  onGenerateBarcode(form: NgForm){
      console.log(form.value);
  }

  create(){
    this.createdCode = this.qrData;
  }
  scan(){
    this.scanner.scan().then(barcodeData => {
      this.scannedCode = barcodeData.text;
    })
  }

  // convertToPdf(){
  //   let x = document.getElementsByClassName('qr');
  //   let y = x[0].children[0].getAttribute('src');
  //   var docDefinition = {
  //     content:[
  //       { text: <ngx-qrcode [qrc-value]="createdCode"></ngx-qrcode> }
  //     ]
  //   }
  //   this.pdfObj = pdfMake.createPdf(docDefinition);
  // }

  // downloadPdf(){
  //   if(this.plt.is('cordova')){
  //     this.pdfObj.getBuffer((buffer)=>{
  //       var utf8 = new Uint8Array(buffer);
  //       var binaryArray = utf8.buffer;
  //       var blob = new Blob([binaryArray], { type: 'application/pdf'});

  //       this.file.writeFile(this.file.dataDirectory, 'barcode.pdf', blob, { replace: true}).then(fileEntry => {
  //         this.fileOpener.open(this.file.dataDirectory + 'barcode.pdf', 'application/pdf');
  //       })
  //     })
  //   }else{
  //     this.pdfObj.download();
  //   }
  // }

}
