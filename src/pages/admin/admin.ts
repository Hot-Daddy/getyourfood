import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { AdminUserDetailsPage } from '../admin-user-details/admin-user-details';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database-deprecated';

/**
 * Generated class for the AdminPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-admin',
  templateUrl: 'admin.html',
})
export class AdminPage {

  merchantList: FirebaseListObservable<any>
  allArrData = []
  arrData = []
  queryText: string

  constructor(public navCtrl: NavController, public navParams: NavParams, private modalCtrl: ModalController, private fdb: AngularFireDatabase) {
    this.merchantList = this.fdb.list('/pembeli/');
      this.merchantList.subscribe(_data=>{
        console.log(_data);
        
        this.allArrData = _data
        this.arrData = _data;
        console.log(this.arrData);
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminPage');
  }

  modalUser(key, email, username, saldo){
    let modal = this.modalCtrl.create(AdminUserDetailsPage, {
      key: key,
      email: email,
      username: username,
      saldo: saldo
    });
    modal.present();
  }
  searchUser(){
    let queryToLower = this.queryText.toLowerCase();
    this.arrData = []
    this.allArrData.forEach(element => {
      if (queryToLower == "") {
        this.arrData = this.allArrData
      } 
      else if (element.email.toLowerCase().includes(queryToLower)) {
        this.arrData.push(element)
      }
      else if (element.username.toLowerCase().includes(queryToLower)){
        this.arrData.push(element)
      }
    });
    
  }

}
