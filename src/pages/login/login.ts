import { Component , OnInit} from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database-deprecated';
import firebase, { auth } from 'firebase';
import { FormControl, Validators } from '@angular/forms';
import { SideMenuPage } from '../side-menu/side-menu';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  emailLogin: any
  passwordLogin: any
  emailRegister:any
  passwordRegister:any
  repasswordRegister:any
  LogRes:any
  loadingController:any
  loading:any

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private auth: AngularFireAuth, private loadingCtrl: LoadingController, 
    private fdb: AngularFireDatabase, private alertCtrl: AlertController) {
  this.loadingController = loadingCtrl;

  }

  ionViewDidLoad() {
    
    console.log('ionViewDidLoad LoginPage');
    this.LogRes = "Login";
  }
  async login() {
    this.presentLoadingText()
    let emailReq = new FormControl(this.emailLogin, Validators.required)
    let passwordReq = new FormControl(this.emailLogin, Validators.required)
    console.log(passwordReq);
    
    if (emailReq.status == "INVALID" || passwordReq.status == "INVALID") {
      let alert = this.alertCtrl.create({
        title: 'Login failed',
        message: 'Please fill up your email and password',
        buttons: ['OK']
      });
      alert.present();
    } else {
      this.auth.auth.signInWithEmailAndPassword(this.emailLogin,this.passwordLogin) 
      .then((user)=>{
        this.navCtrl.setRoot(SideMenuPage)
      }) 
      .catch((error)=>{
        let alert = this.alertCtrl.create({
          title: 'Login failed',
          message: error,
          buttons: ['OK']
        });
        alert.present();
      })
    }
    
    this.loading.dismiss()
  }
  async register(): Promise<any> {
    this.presentLoadingText()
      let emailReq = new FormControl(this.emailRegister, Validators.required)
      let passwordReq = new FormControl(this.passwordRegister, Validators.required)
      let repasswordReq = new FormControl(this.repasswordRegister, Validators.required)
    if (emailReq.status == "INVALID" || passwordReq.status == "INVALID" || repasswordReq.status == "INVALID") {
      let alert = this.alertCtrl.create({
        title: 'Register failed',
        message: 'Please fill up your email and password',
        buttons: ['OK']
      });
      alert.present();
    } else if (this.passwordRegister != this.repasswordRegister) {
      let alert = this.alertCtrl.create({
        title: 'Register failed',
        message: 'Please check your credential',
        buttons: ['OK']
      });
      alert.present();
    } else {
      firebase.auth().createUserWithEmailAndPassword(this.emailRegister,this.passwordRegister)
      .then ((user) => {
        console.log(user);
        
        firebase.database().ref('/pembeli/'+ user.uid).set({
          "email": this.emailRegister,
          "saldo": "0",
          "username": "-"
        }, ()=>{})
        firebase.database().ref('users').child(user.uid).set({
          "privillage": "pembeli"
        })
        this.navCtrl.setRoot(SideMenuPage)
      })
      .catch ((error) => {
        console.log(error);
        
        let alert = this.alertCtrl.create({
          title: 'Register failed',
          message: error,
          buttons: ['OK']
        });
        alert.present();
      }) 
    }
    this.loading.dismiss()
  }
  presentLoadingText() {
    this.loading = this.loadingCtrl.create({
      content: `<ion-spinner name="bubbles">Loading</ion-spinner>`
    });
    this.loading.present();
  }
  
}