import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController, Platform, ActionSheetController, ViewController } from 'ionic-angular';
import { NgForm, FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database-deprecated';
import { AdminHomePage } from '../admin-home/admin-home';
import firebase from 'firebase';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FirebaseApp } from 'angularfire2';
import { auth } from 'firebase';
import { AdminPage } from '../admin/admin';
import { SideMenuPage } from '../side-menu/side-menu';
/**
 * Generated class for the AdminAddMerchantPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-admin-add-merchant',
  templateUrl: 'admin-add-merchant.html',
})
export class AdminAddMerchantPage {

  addMerchant: FormGroup;

  arrData = [];
  selectedPhoto;
  loading;
  currentImage;
  imageName;
  imageUrl = '';
  base64Image:String;
  imageToUpload:String;

  merchantList: FirebaseListObservable<any>
  userList: FirebaseListObservable<any>
  email: any
  password: any
  namaToko: any
  saldo: any
  description: any

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public fb: FormBuilder,
              private afAuth: AngularFireAuth,
              public fdb: AngularFireDatabase,
              public toastCtrl: ToastController,
              public camera: Camera,
              public loadingCtrl:LoadingController,
              public platform: Platform,
              public actionsheetCtrl: ActionSheetController,
              public firebase: FirebaseApp,
              public view: ViewController
              ) {

      this.addMerchant = fb.group({
          email: new FormControl('', Validators.required),
          password: new FormControl('', Validators.required),
          confirmPassword: new FormControl('', Validators.required),
          namaToko: new FormControl('', Validators.required),
          saldo: new FormControl('', Validators.required),
          desc: new FormControl('', Validators.required),
        }, 
        {validator: AdminAddMerchantPage.passwordsMatch}

      );    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminAddMerchantPage');
  }

  async onAddMerchant(): Promise<any>{
    await this.afAuth.auth.createUserWithEmailAndPassword(this.addMerchant.value.email,this.addMerchant.value.password)
    .then ((user) => {
      this.merchantList = this.fdb.list('/penjual/');
      // this.userList = this.fdb.list()
      this.merchantList.subscribe(_data=>{
        this.arrData = _data;
      });
      firebase.database().ref('/penjual/'+ user.uid).set({
        namaToko: this.addMerchant.value.namaToko,
        saldo: this.addMerchant.value.saldo,
        deskripsi: this.addMerchant.value.desc,
        email: this.addMerchant.value.email
      })
      firebase.database().ref('users').child(user.uid).set({
        "privillage": "penjual"
      })
      firebase.auth().signOut()
      this.afAuth.auth.signInWithEmailAndPassword("admin1@mail.com","admin1")
      this.toastCtrl.create({
        message: "Success addedd New Store",
        duration: 5000
      }).present();
      this.navCtrl.setRoot(SideMenuPage)
    })
    .catch ((error) => {
      this.toastCtrl.create({
        message: error,
        duration: 5000
      }).present();
    });


  }

  openMenu() {
    let actionSheet = this.actionsheetCtrl.create({
      title: 'Choose a photo',
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Gallery',
          role: 'destructive',
          icon: !this.platform.is('ios') ? 'book' : null,
          handler: () => {
            this.takePict();
          }
        },
        {
          text: 'Take a Photo',
          icon: !this.platform.is('ios') ? 'camera' : null,
          handler: () => {
            this.onTakePhoto(); 
          }
        },
        {
          text: 'Cancel',
          role: 'cancel', // will always sort to be on the bottom
          icon: !this.platform.is('ios') ? 'close' : null,
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  onError = (error) =>{
    console.log(error)
    this.loading.dismiss();
  }

  upload(){
    var uploadTask = this.firebase.storage().ref().child('/images/'+ auth().currentUser.uid 
    +'.jpg').put(this.base64Image);
    uploadTask.then(this.onError)
    // uploadTask.then(data=>{
    //   this.toast.create({
    //     message: uploadTask.snapshot.downloadURL.toLowerCase(),
    //     duration: 2000
    //   }).present();
    // })
    
    
  }

  takePict(){
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture({
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth:400,
      targetHeight:200,
      sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM
    }).then((imageData)=>{
      this.base64Image = 'data:image/jpeg;base64,'+imageData;
      this.imageToUpload = imageData;
    },(err)=>{
      console.log(err);
    })
  }

  onTakePhoto() { 
    const options: CameraOptions = {
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
    }

    this.camera.getPicture(options).
    then((imageData) => {
        this.base64Image = 'data:image/jpeg;base64,'+ imageData;
        console.log(this.imageUrl);
      }, 
      (err) => {
        console.log(err);
    });
  }

  dataURLtoBlob(dataUrl){
    let binary = atob(dataUrl.split(',')[1])
    let tamp = [];
    for(var index = 0; index < binary.length; index++){
      tamp.push(binary.charAt(index));
    }
    return new Blob([new Uint8Array(tamp)], {type:'image/jpeg'})
  }


  static passwordsMatch(cg: FormGroup): {[err: string]: any} {
    let password = cg.get('password');
    let confirmPassword = cg.get('confirmPassword');
    let rv: {[error: string]: any} = {};
    if ((password.touched || confirmPassword.touched) && password.value !== confirmPassword.value) {
      rv['passwordMismatch'] = true;
    }
    return rv;
  }

  

}
