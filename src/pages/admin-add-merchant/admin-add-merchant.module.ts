import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminAddMerchantPage } from './admin-add-merchant';

@NgModule({
  declarations: [
    AdminAddMerchantPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminAddMerchantPage),
  ],
})
export class AdminAddMerchantPageModule {}
