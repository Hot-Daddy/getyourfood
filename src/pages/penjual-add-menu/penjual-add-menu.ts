import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController, ToastController } from 'ionic-angular';
import { CameraOptions, Camera } from '@ionic-native/camera';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase'; 
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database-deprecated';
import { FirebaseApp } from 'angularfire2';
import { HomePage } from '../home/home';
import { auth } from 'firebase';

import { Platform, ActionSheetController } from 'ionic-angular';


/**
 * Generated class for the PenjualAddMenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-penjual-add-menu',
  templateUrl: 'penjual-add-menu.html',
})
export class PenjualAddMenuPage {
  arrData = [];
  selectedPhoto;
  loading;
  currentImage;
  imageName;
  imageUrl = '';
  base64Image:String;
  imageToUpload:String;
  menuList: FirebaseListObservable<any>
 
  name: any
  menu: any
  harga: any
  desc: any

  constructor(public navCtrl: NavController,private afAuth: AngularFireAuth, 
    public navParams: NavParams, public viewCtrl: ViewController, public camera: Camera, 
    public fdb: AngularFireDatabase, public loadingCtrl:LoadingController, public firebase: FirebaseApp, public platform: Platform,
    public actionsheetCtrl: ActionSheetController) {

    this.afAuth.authState.subscribe(data=>{
      if(data.email){
        console.log(data);
        
        this.menuList = fdb.list('/menu/'+data.uid)
        }
      })
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PenjualAddMenuPage');
  }

  closeModal(){
    this.viewCtrl.dismiss();
  }


  btnAddMenu(menu, harga, desc){
    // this.upload();
    this.menuList.push({
      menu : menu,
      harga : harga,
      deskripsi : desc,
      // image : this.imageToUpload
    }).then(newMenu =>{
      this.viewCtrl.dismiss();
    }, error=>{console.log(error)});
  }

  upload(){
      var uploadTask = this.firebase.storage().ref().child('/images/'+ auth().currentUser.uid 
      +'.jpg').put(this.base64Image);
      uploadTask.then(this.onError)
      // uploadTask.then(data=>{
      //   this.toast.create({
      //     message: uploadTask.snapshot.downloadURL.toLowerCase(),
      //     duration: 2000
      //   }).present();
      // })
      
      
  }

  onError = (error) =>{
    console.log(error)
    this.loading.dismiss();
  }

  takePict(){
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture({
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth:400,
      targetHeight:200,
      sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM
    }).then((imageData)=>{
      this.base64Image = 'data:image/jpeg;base64,'+imageData;
      this.imageToUpload = imageData;
    },(err)=>{
      console.log(err);
    })
  }

  onTakePhoto() { 
    const options: CameraOptions = {
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
    }

    this.camera.getPicture(options).
    then((imageData) => {
        this.base64Image = 'data:image/jpeg;base64,'+ imageData;
        console.log(this.imageUrl);
      }, 
      (err) => {
        console.log(err);
    });
  }

  dataURLtoBlob(dataUrl){
    let binary = atob(dataUrl.split(',')[1])
    let tamp = [];
    for(var index = 0; index < binary.length; index++){
      tamp.push(binary.charAt(index));
    }
    return new Blob([new Uint8Array(tamp)], {type:'image/jpeg'})
  }

  openMenu() {
    let actionSheet = this.actionsheetCtrl.create({
      title: 'Choose a photo',
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Gallery',
          role: 'destructive',
          icon: !this.platform.is('ios') ? 'book' : null,
          handler: () => {
            this.takePict();
            
          }
        },
        {
          text: 'Take a Photo',
          icon: !this.platform.is('ios') ? 'camera' : null,
          handler: () => {
            this.onTakePhoto(); 
          }
        },
        {
          text: 'Cancel',
          role: 'cancel', // will always sort to be on the bottom
          icon: !this.platform.is('ios') ? 'close' : null,
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }
}
