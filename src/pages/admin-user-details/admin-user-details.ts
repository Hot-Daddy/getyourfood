import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController, AlertController } from 'ionic-angular';
import { FirebaseListObservable, AngularFireDatabase } from 'angularfire2/database-deprecated';

/**
 * Generated class for the AdminUserDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-admin-user-details',
  templateUrl: 'admin-user-details.html',
})
export class AdminUserDetailsPage {

  id: any
  email: any
  username: any
  saldo: any
  penjual : FirebaseListObservable<any>

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              private viewCtrl: ViewController, 
              private fdb: AngularFireDatabase, 
              private toast: ToastController,
              private alertCtrl: AlertController) {
    this.penjual = fdb.list('/pembeli')
    this.id = this.navParams.get('key')
    this.email = this.navParams.get('email')
    this.username = this.navParams.get('username')
    this.saldo = this.navParams.get('saldo')
    console.log(this.saldo);
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminUserDetailsPage');
  }

  closeModal(){
    this.viewCtrl.dismiss();
  }

  btnUpdateMenu(id, username, saldo){
    this.penjual.update(id = this.id, {
      username : this.username ,
      saldo : this.saldo
    }).then(updateMenu =>{
      this.viewCtrl.dismiss();
      this.toast.create({
        message: `Data Updated`,
        duration: 1000
      }).present(); 
    }, error=>{console.log(error)});
  }

  alertAddSaldo(id,saldo) {
    let alert = this.alertCtrl.create({
      title: 'Topup Saldo',
      message: 'Saldo Saat Ini : ' + saldo,
      inputs: [
        {
          name: 'saldoTopup',
          value: 'Jumlah Topup Saldo',
          type: 'number'
        },
        {
          name: 'idUser',
          value: id,
          type: 'hidden'
        },
        {
          name: 'saldoDB',
          value: saldo,
          type: 'hidden'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Confirm',
          handler: data => {
            console.log(data.saldoDB, data.saldoTopup);
            var saldoAwal = parseInt(data.saldoDB);
            var saldoInput = parseInt(data.saldoTopup);
            var saldoInsert = saldoAwal + saldoInput;
            console.log(saldoInsert);
            if(saldoInsert >= 0){
              this.penjual.update(id = data.idUser, {
                saldo: saldoInsert
              }).then(updateMenu =>{
                this.viewCtrl.dismiss();
                this.toast.create({
                  message: `Berhasil Tarik Saldo`,
                  duration: 3000
                }).present(); 
                //this.viewCtrl.dismiss();

              }, error=>{console.log(error)});
            }
            else{
              this.viewCtrl.dismiss();
              this.toast.create({
                message: `Gagal Topup Saldo`,
                duration: 3000
              }).present(); 
              
            }
            
          }
        }
      ],
      enableBackdropDismiss: false 
    });
    
    alert.present();
  }


}
