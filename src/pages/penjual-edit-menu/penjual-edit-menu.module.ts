import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PenjualEditMenuPage } from './penjual-edit-menu';

@NgModule({
  declarations: [
    PenjualEditMenuPage,
  ],
  imports: [
    IonicPageModule.forChild(PenjualEditMenuPage),
  ],
})
export class PenjualEditMenuPageModule {}
