import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DeskripsiModalPage } from './deskripsi-modal';

@NgModule({
  declarations: [
    DeskripsiModalPage,
  ],
  imports: [
    IonicPageModule.forChild(DeskripsiModalPage),
  ],
})
export class DeskripsiModalPageModule {}
